import h5py
import numpy as np

from bokeh.io import output_notebook

from scripts.io import load_spectra
from scripts.visualization import plot_line

output_notebook()
