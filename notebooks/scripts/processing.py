import numpy as np


def normalize(x):
    baseline = np.percentile(x, 10)
    span = x.max() - baseline
    return (x - baseline)/span
