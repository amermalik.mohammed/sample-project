from bokeh.plotting import figure, show


def plot_line(*data, plot_kwargs={}, fig_kwargs={}, show_plot=True):

    if len(data) == 1:
        y = data[0]
        x = range(len(y))
    elif len(data) == 2:
        x, y = data
    else:
        raise ValueError('Max. 2 positional arguments are supported.')

    tools = 'box_zoom,pan,save,hover,reset,tap,wheel_zoom'
    default_figure_kwargs = {'width': 800, 'height': 400, 'tools': tools}
    default_figure_kwargs.update(fig_kwargs)

    fig = figure(**default_figure_kwargs)
    fig.line(x, y, **plot_kwargs)
    if show_plot:
        show(fig)
    return fig
